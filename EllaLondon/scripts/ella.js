/*-----------------------------------------------------------------------------------*/
/*	SLIDER
/*-----------------------------------------------------------------------------------*/

$(document).ready(function () {

    if ($.fn.cssOriginal != undefined)
        $.fn.css = $.fn.cssOriginal;

    $('.fullwidthbanner').revolution(
						{
						    delay: 9000,
						    startwidth: 960,
						    startheight: 450,
						    hideThumbs: 200,
						    onHoverStop: "on", 					// Stop Banner Timet at Hover on Slide on/off
						    navigationType: "none", 				//bullet, thumb, none, both	 (No Shadow in Fullwidth Version !)
						    touchenabled: "on", 					// Enable Swipe Function : on/off
						    fullWidth: "on"

						});
});


$(document).ready(function () {

    if ($.fn.cssOriginal != undefined)
        $.fn.css = $.fn.cssOriginal;

    $('.banner').revolution(
						{
						    delay: 9000,
						    startheight: 450,
						    startwidth: 960,

						    hideThumbs: 200,

						    navigationType: "bullet", 				//bullet, thumb, none, both		(No Thumbs In FullWidth Version !)
						    navigationArrows: "verticalcentered", 	//nexttobullets, verticalcentered, none
						    navigationStyle: "round", 			//round,square,navbar

						    touchenabled: "on", 					// Enable Swipe Function : on/off
						    onHoverStop: "on", 					// Stop Banner Time at Hover on Slide on/off

						    navOffsetHorizontal: 0,
						    navOffsetVertical: -25,

						    shadow: 1, 							//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
						    fullWidth: "off"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus

						});

});